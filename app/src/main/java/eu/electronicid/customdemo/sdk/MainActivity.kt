package eu.electronicid.customdemo.sdk

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import eu.electronicid.customdemo.R
import eu.electronicid.customdemo.sdk.custom.CustomVideoIdActivity
import eu.electronicid.customdemo.sdk.custom.CustomVideoIdActivity2
import eu.electronicid.customdemo.sdk.custom.CustomVideoIdActivity3
import eu.electronicid.sdk.videoid.VideoIDActivity
import eu.electronicid.sdklite.video.config.Environment
import kotlinx.android.synthetic.main.activity_main.button_start_sample
import java.net.URL

class MainActivity : AppCompatActivity() {

    private val endpoint = URL("https://etrust-sandbox.electronicid.eu/v2/")

    private enum class TypeCustomView { STYLE, CUSTOM_1, CUSTOM_2, VIDEO_CONFERENCE }

    private var typeCustomView = TypeCustomView.VIDEO_CONFERENCE

    private val videoIDActivityLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            onVideoIdResult(resultCode = it.resultCode, data = it.data)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button_start_sample.setOnClickListener {
            val intent = Intent(
                this, when (typeCustomView) {
                    TypeCustomView.STYLE -> VideoIDActivity::class.java
                    TypeCustomView.CUSTOM_1 -> CustomVideoIdActivity::class.java
                    TypeCustomView.CUSTOM_2 -> CustomVideoIdActivity2::class.java
                    TypeCustomView.VIDEO_CONFERENCE -> CustomVideoIdActivity3::class.java
                }
            ).apply {
                putExtra(VideoIDActivity.INTENT_ENVIRONMENT, Environment(endpoint, "{AUTH}"))
                putExtra(VideoIDActivity.INTENT_LANGUAGE, "en")
                putExtra(VideoIDActivity.INTENT_DOCUMENT_TYPE, 62)
            }
            videoIDActivityLauncher.launch(intent)
        }
    }

    private fun onVideoIdResult(resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_OK) {
            data?.run {
                val videoId = getStringExtra(VideoIDActivity.INTENT_RESULT_OK)

                Toast.makeText(this@MainActivity, "Video OK: $videoId", Toast.LENGTH_SHORT).show()
            }
        } else if (resultCode == RESULT_CANCELED) {
            data?.run {
                val errorId = getStringExtra(VideoIDActivity.INTENT_RESULT_ERROR_CODE)
                val errorMsg = getStringExtra(VideoIDActivity.INTENT_RESULT_ERROR_MESSAGE)

                Toast.makeText(this@MainActivity, "Video ERROR id: $errorId, msg: $errorMsg", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }
}
